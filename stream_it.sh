#!/bin/bash
# stream_it.sh
# Record fm channel. With a SDR dongle and raspberry pi. 
# Written as 
#crontab -e
# add:
#     0 16 * * 1-5 /usr/local/bin/stream_it.sh >/dev/null 2>&1
# install default software.
#   sudo apt -y install sox rtl_sdr
# Tricks needed to get decent sound quality.
# -d 0 Select the right device 
# -M wbfm  wideband mode as the signal is not narrow.
# -s 240000 limit the sample size.  
# -r 48k resample to standard size 
# -F 4 filter   Filter and Gain are the how to 
# -g 150 gain.  remove static and enable smooth recording.
# Weather channels:
# 162.400 MHz 162.425 MHz 162.450 MHz 162.475 MHz 162.500 MHz 162.525 MHz 162.550 MHz

CHANNEL='88.1M'
SAVE_DIR='/shows/'
SHOW='4PM'
DATE=`date +"%Y-%m-%d"`
FILENAME="${DATE}-${SHOW}.ogg"
START='0'
LENGTH='30:15'

mkdir -p ${SAVE_DIR}

/usr/bin/rtl_fm -d 0 -M wbfm -f ${CHANNEL} -s 240000 -r 48k -F 4 -g 150  - | /usr/bin/sox -r 48k -t raw -e signed -b 16 -c 1 -V1 - ${SAVE_DIR}${FILENAME} trim ${START} ${LENGTH}
